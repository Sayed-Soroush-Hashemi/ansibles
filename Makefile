requirements:
	# make isn't installed yet, so you have to run the first line manually
	sudo apt-get install make python3-apt
	sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
	sudo apt-add-repository "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main"
	sudo apt-get update
	sudo apt-get install ansible

create-efi-bootable-usb:
	# Don't forget to set the device in the vars section in the playbook
	$(ANSIBLE_PLAYBOOK) create-efi-bootable-usb.yml

create-bios-bootable-usb:
	# Don't forget to set the device in the vars section in the playbook
	$(ANSIBLE_PLAYBOOK) create-bios-bootable-usb.yml

install-all: install-gcc \
	install-python \
	install-gcc \
	insdall-java \
	install-go \
	install-vim \
	install-sublime-text \
	install-git \
	install-minio \
	install-dvc \
	install-docker \
	install-docker-compose \
	install-kubectl \
	install-skype \
	install-discord \
	install-telegram \
	install-slack \
	install-chrome \
	install-openvpn-client \
	install-anyconnect-client \
	install-wireshark \
	install-terminal-tools \
	install-monitoring-tools \
	install-anydesk \
	install-i3 \
	install-postman
# TODO: openvpn, dvc (had some issues with pyyaml), wireshark

install-gcc:
	$(ANSIBLE_PLAYBOOK) install-gcc.yml

install-python:
	$(ANSIBLE_PLAYBOOK) install-python.yml

install-java:
	$(ANSIBLE_PLAYBOOK) install-java.yml

install-go:
	$(ANSIBLE_PLAYBOOK) install-go.yml

install-vim:
	$(ANSIBLE_PLAYBOOK) install-vim.yml

install-sublime-text:
	$(ANSIBLE_PLAYBOOK) install-sublime-text.yml

install-git:
	$(ANSIBLE_PLAYBOOK) install-git.yml

install-minio:
	$(ANSIBLE_PLAYBOOK) install-minio.yml

install-dvc:
	$(ANSIBLE_PLAYBOOK) install-dvc.yml

install-docker:
	$(ANSIBLE_PLAYBOOK) install-docker.yml

install-docker-compose:
	$(ANSIBLE_PLAYBOOK) install-docker-compose.yml

install-kubectl:
	$(ANSIBLE_PLAYBOOK) install-kubectl.yml

install-skype:
	$(ANSIBLE_PLAYBOOK) install-skype.yml

install-discord:
	$(ANSIBLE_PLAYBOOK) install-discord.yml

install-telegram:
	$(ANSIBLE_PLAYBOOK) install-telegram.yml

install-slack:
	$(ANSIBLE_PLAYBOOK) install-slack.yml

install-chrome:
	$(ANSIBLE_PLAYBOOK) install-chrome.yml

install-openvpn-client:
	$(ANSIBLE_PLAYBOOK) install-openvpn-client.yml

install-anyconnect-client:
	$(ANSIBLE_PLAYBOOK) install-anyconnect-client.yml

install-wireshark:
	$(ANSIBLE_PLAYBOOK) install-wireshark.yml

install-terminal-tools:
	$(ANSIBLE_PLAYBOOK) install-terminal-tools.yml

install-monitoring-tools:
	$(ANSIBLE_PLAYBOOK) install-monitoring-tools.yml

install-anydesk:
	$(ANSIBLE_PLAYBOOK) install-anydesk.yml

install-i3:
	$(ANSIBLE_PLAYBOOK) install-i3.yml

install-pycharm:
	$(ANSIBLE_PLAYBOOK) install-pycharm.yml

install-goland:
	$(ANSIBLE_PLAYBOOK) install-goland.yml

install-clion:
	$(ANSIBLE_PLAYBOOK) install-clion.yml

install-intellij:
	$(ANSIBLE_PLAYBOOK) install-intellij.yml

install-postman:
	$(ANSIBLE_PLAYBOOK) install-postman.yml

###############################################################################
ANSIBLE_PLAYBOOK := ansible-playbook --ask-become-pass --
